package com.lern.calculatormvc.view;

import com.lern.calculatormvc.R;

/**
 * Created by mark on 30.05.14.
 */
public class IdArrays {

    public static final int[] ID = {
            R.id.equalButtonId,
            R.id.divisionId,
            R.id.additionId,
            R.id.subtractionId,
            R.id.multiplicationId
    };
}
