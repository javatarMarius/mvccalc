package com.lern.calculatormvc.controller;

import android.app.Activity;
import android.view.View;
import com.lern.calculatormvc.R;
import com.lern.calculatormvc.model.ModelCalculator;
import com.lern.calculatormvc.view.ViewCalculator;

public class CalculatorController {

    private ViewCalculator mViewCalculator;
    private ModelCalculator mModelCalculator;

    public CalculatorController(Activity activity) {
        mModelCalculator = new ModelCalculator();
        mViewCalculator = new ViewCalculator(activity);
        this.mViewCalculator.execute(new CalculateListener());
    }

    private int getFirstNumber() {
        return mViewCalculator.getFirstNumber();
    }

    private int getSecondNumber() {
        return mViewCalculator.getSecondNumber();
    }

    private void calculate() {
        final String tempOperator = mViewCalculator.getOperatorSymbolId();

        if (tempOperator.equals("+")) {
            mModelCalculator.addition(getFirstNumber(), getSecondNumber());
            mViewCalculator.setAnsver(mModelCalculator.getSumResult());
        } else if (tempOperator.equals("-")) {
            mModelCalculator.subtraction(getFirstNumber(), getSecondNumber());
            mViewCalculator.setAnsver(mModelCalculator.getSubtractionResult());
        } else if (tempOperator.equals("*")) {
            mModelCalculator.multiplication(getFirstNumber(), getSecondNumber());
            mViewCalculator.setAnsver(mModelCalculator.getMultiplicationResult());
        } else if (tempOperator.equals("/")) {
            if (mModelCalculator.division(getFirstNumber(), getSecondNumber())) {
                mViewCalculator.setAnsver(mModelCalculator.getDivisionResult());
            } else {
                mViewCalculator.showDivisionError();
            }
        }
    }

    private class CalculateListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.additionId:
                    mViewCalculator.setOperatorSymbolId("+");
                    break;
                case R.id.subtractionId:
                    mViewCalculator.setOperatorSymbolId("-");
                    break;
                case R.id.multiplicationId:
                    mViewCalculator.setOperatorSymbolId("*");
                    break;
                case R.id.divisionId:
                    mViewCalculator.setOperatorSymbolId("/");
                    break;
                case R.id.equalButtonId:
                    calculate();
                    break;
            }
        }
    }
}
