package com.lern.calculatormvc;

import android.app.Activity;
import android.os.Bundle;
import com.lern.calculatormvc.controller.CalculatorController;
import com.lern.calculatormvc.model.ModelCalculator;
import com.lern.calculatormvc.view.ViewCalculator;

public class CalculatorTest extends Activity {
    public static final String TAG = "myInfo";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        CalculatorController calculatorController = new CalculatorController(CalculatorTest.this);
    }
}
