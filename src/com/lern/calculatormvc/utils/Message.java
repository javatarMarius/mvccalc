package com.lern.calculatormvc.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import com.lern.calculatormvc.R;

/**
 * Created by mark on 03.06.14.
 */
public class Message {

    public static AlertDialog.Builder alert(Activity activity, final int id) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
        alertDialog.setTitle(id);
        alertDialog.setCancelable(false);
        return alertDialog;
    }
}
